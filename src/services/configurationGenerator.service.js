import { Configuration } from "../database/models/configuration.model.js"
import { logger } from '../helpers/logger.js'
export class ConfigurationService {

    async createConfiguration(maxValue, minValue, minGasPrice, maxGasPrice, minPriorityFeePerGas, maxPriorityFeePerGas) {
        try {
            const configurationParams = {
                maxValue: maxValue ,
                minValue: minValue,
                minGasPrice: minGasPrice,
                maxGasPrice: maxGasPrice,
                minPriorityFeePerGas: minPriorityFeePerGas,
                maxPriorityFeePerGas: maxPriorityFeePerGas,
                isDeleted: false
            }
            const config = Configuration.build(configurationParams);
            await config.save();
            logger.info(`Successfully created configuration!`)
            return `Successfully created configuration!`
        } catch(e) {
            console.error(`An error occurred createConfiguration :: ${e}`);
        }
    }

    async updateConfiguration(id, maxValue, minValue, minGasPrice, maxGasPrice, minPriorityFeePerGas, maxPriorityFeePerGas) {
        try {
            const configId = id;
            const configurationParams = {
                maxValue: maxValue ,
                minValue: minValue,
                minGasPrice: minGasPrice,
                maxGasPrice: maxGasPrice,
                minPriorityFeePerGas: minPriorityFeePerGas,
                maxPriorityFeePerGas: maxPriorityFeePerGas,
                isDeleted: false
            };
            const configuration = await Configuration.findOne({where: {id: configId}});
            logger.info(`Successfully updated configuration :: ${id}`)
            const result = await configuration.update(configurationParams);
            return result;

        } catch(e) {
            logger.error(`An error occurred updateConfiguration :: ${e}`);
        }
    }

    async getConfigurations() {
        try {
            const configurations = await Configuration.findAll({where: {isDeleted: false}});
            return configurations;
        } catch(e) {
            logger.error(`An error occurred getConfigurations :: ${e}`);
        }
    }

    async softDeleteConfiguration(id) {
        try {
            const configId = id;
            const configurationParams = {
                isDeleted: true
            }
            const configuration = await Configuration.findOne({where: {id: configId}});
            await configuration.update(configurationParams);
            logger.info(`Configuration successfully deleted! ${id}`)
            return `Configuration successfully deleted!`
        } catch(e) {
            logger.error(`An error occurred softDelete :: ${e}`);
        }
    }
    //TODO use CASCADE to delete a configuration and all its relatations (transactions associated with it)
    async hardDeleteConfiguration() {
        try {
            console.log('test')
        } catch(e) {
            logger.error(`An error occurred hardDelete :: ${e}`);
        }
    }
}