import Web3 from "web3";
import * as dotenv from "dotenv";
import { logger } from '../helpers/logger.js'
import { subscriptionTypes } from "../types/ethTransaction.types.js";
import { ActiveConfiguration } from "../database/models/activeConfig.model.js";
import { Configuration } from "../database/models/configuration.model.js";
import { Transaction } from "../database/models/transaction.model.js";
//required to use __dirname
import path from "path";
import { fileURLToPath } from "url";
//__dirname is not executable using es6 so we do this to use it
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
//add path to properly load the .env file
dotenv.config({ path: path.resolve(__dirname, "../../.env") });

export class EthTransactionService {
  provider;
  web3Provider;
  web3;
  activeConfiguration;

  constructor() {
    //use websocket url to get continues information about transactions
    this.provider = `wss://mainnet.infura.io/ws/v3/${process.env.INFURA_KEY}`;
    this.web3Provider = new Web3.providers.WebsocketProvider(this.provider);
    this.web3 = new Web3(this.web3Provider);
    this.initializeActiveConfiguration();
    this.getBlockData();
  }

  async getBlockNumber() {
    try {
      //Subscribes to incoming block headers. This can be used as a timer to check for changes on the blockchain.
      const subscription = this.web3.eth.subscribe(
        subscriptionTypes.newBlockHeaders,
        (err, result) => {
          if (err) {
            logger.error(`error with web3 subscription method:: ${err}`);
          }
          console.log(result);
          return {
            number: result.number,
            hash: result.hash,
          };
        }
      );
      return subscription;
    } catch (e) {
      logger.error(`An error occurred getBlock:: ${e}`);
    }
  }
  async getBlockData() {
    try {
      let blockTransactions;
      let transactionCount;
      const blockInfo = await this.getBlockNumber();
      blockInfo.on("data", async (event) => {
        if (this.activeConfiguration) {
          blockTransactions = await this.web3.eth.getBlock(event.number); //block metadata
          transactionCount = await this.getTransactionCount(event.hash); // a number of all transaction in the block
          if (blockTransactions) {
            console.log(transactionCount);
            console.log(blockTransactions);
            blockTransactions.transactions.forEach(async (transaction) => {
              const transactionData = await this.getTransactionData(
                transaction
              );
              // console.log(await this.getTransactionData(transaction)); //transaction metadata
              this.filterTransactions(transactionData);
            });
          }
        }
      });
      return {
        blockTransactions,
        transactionCount,
      };
    } catch (e) {
      logger.error(`An error occurred getTransaction :: ${e}`);
    }
  }

  async getTransactionCount(blockHash) {
    try {
      const count = await this.web3.eth.getBlockTransactionCount(blockHash);
      return count;
    } catch (e) {
      logger.error(`An error occurred:: ${e}`);
    }
  }

  async getTransactionData(hash) {
    try {
      const result = this.web3.eth.getTransaction(hash);
      return result;
    } catch (e) {
      logger.error(`An error occurred getTransactionData:: ${e}`);
    }
  }

  //TODO Implement Filter Function which stores transactions which meet configuration requirements
  async filterTransactions(transactionData) {
    try {
      //check if activeConfiguration matches transactionData
      //apply some random rule from the configuration using its parameters
      //this condition is used only for illustration purposes since we have no hard rules on how to use the configuration in the assignment
      if (transactionData.gasPrice && transactionData.gasPrice > this.activeConfiguration.maxGasPrice) {

        const params = {
          blockHash: transactionData.blockHash,
          hash: transactionData.hash,
          nonce: transactionData.nonce,
          blockNumber: transactionData.blockNumber,
          transactionIndex: transactionData.transactionIndex,
          value: transactionData.value,
          gas: transactionData.gas,
          gasPrice: transactionData.gasPrice,
          from: transactionData.from,
          to: transactionData.to,
          maxFeePerGas: transactionData.maxFeePerGas,
          maxPriorityFeePerGas: transactionData.maxPriorityFeePerGas,
          input: transactionData.input,
          configurationId: this.activeConfiguration.id,
        };

        await Transaction.create(params);
      }
    } catch (e) {
      logger.error(`An error occurred filter :: ${e}`);
    }
  }

  async initializeActiveConfiguration() {
    try {
      const activeConfig = await ActiveConfiguration.findOne();
      if (activeConfig) {
        this.activeConfiguration = await Configuration.findOne({
          where: { id: activeConfig.configurationId },
        });
      }
    } catch (e) {
      logger.error(`An error occurred initActiveConfig :: ${e}`);
    }
  }
}
