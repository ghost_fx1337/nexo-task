
import { Configuration } from "../database/models/configuration.model.js";
import { ActiveConfiguration } from '../database/models/activeConfig.model.js';
import { logger } from '../helpers/logger.js'
export class ConfigurationLoaderService {
    ethService;
    constructor({ ethTransactionService }) {
        this.ethService = ethTransactionService;
    }
    async setActiveConfig(id) {
        const configId = id;
        try {
            const configuration = await Configuration.findOne({where: {id: configId}});

            if(!configuration) {
                logger.error(`No such configuration`);
            }
            const params = {
                configurationId: configId,
                isActive: true
            }
            
            const activeConfig = await ActiveConfiguration.findOne();
            console.log(activeConfig);
            if(!activeConfig) {
                await ActiveConfiguration.create(params);
            } else {
                await activeConfig.update(params);
            }
  
            this.ethService.initializeActiveConfiguration();
            logger.info(`Successfuly set configuration!`);
            return `Successfully set configuration!`;
        } catch(e) {
            logger.error(`An error occurred activeConfiguration :: ${e}`);
        }
    }

}