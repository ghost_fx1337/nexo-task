import awilix from "awilix";
import { ActiveConfigurationController } from "../controllers/activeConfiguration.controller.js";
import { ConfigurationController } from "../controllers/configuration.controller.js";
import { ConfigurationLoaderService } from "../services/activeConfiguration.service.js";
import { ConfigurationService } from "../services/configurationGenerator.service.js";
import { EthTransactionService } from "../services/ethTransaction.service.js";

export const container = awilix.createContainer({
  injectionMode: awilix.InjectionMode.PROXY,
});

container.register({
  activeConfigurationController: awilix.asClass(ActiveConfigurationController),
  configurationController: awilix.asClass(ConfigurationController),
  configurationLoaderService: awilix.asClass(ConfigurationLoaderService),
  configurationService: awilix.asClass(ConfigurationService),
  ethTransactionService: awilix.asClass(EthTransactionService),
})

