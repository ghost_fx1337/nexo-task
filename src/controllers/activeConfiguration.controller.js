import { statusCodes } from "../helpers/statusCodes.js";
import { logger } from "../helpers/logger.js";
export class ActiveConfigurationController {
  activeConfigService;
  constructor({ configurationLoaderService }) {
    this.activeConfigService = configurationLoaderService;
  }
  async setConfiguration(req, res, next) {
    try {
      if (!req.params.id) {
        res.status(statusCodes.HTTP_BAD_REQUEST).send({
          message: "Missing id from params!",
        });
      }
      const { id } = req.params;

      const response = await this.activeConfigService.setActiveConfig(id);
      res.status(statusCodes.HTTP_OK).json(response);
    } catch (e) {
      logger.error(`An error occurred setConfiguration :: ${e}`);
    }
  }
}
