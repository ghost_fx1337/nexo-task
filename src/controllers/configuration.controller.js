import { statusCodes } from "../helpers/statusCodes.js";
import { logger } from '../helpers/logger.js'
export class ConfigurationController {
    configService;
    constructor({ configurationService }) {
        this.configService = configurationService
    }
  async createConfiguration(req, res, next) {
    try {
      if (
        !req.body.maxValue ||
        !req.body.minValue ||
        !req.body.minGasPrice ||
        !req.body.maxGasPrice ||
        !req.body.minPriorityFeePerGas ||
        !req.body.maxPriorityFeePerGas
      ) {
        logger.error('Missing body params')
        res.status(statusCodes.HTTP_BAD_REQUEST).send({
          message: "Missing body params!",
        });
      }

      const {
        maxValue,
        minValue,
        minGasPrice,
        maxGasPrice,
        minPriorityFeePerGas,
        maxPriorityFeePerGas,
      } = req.body;
      const response = await this.configService.createConfiguration(
        maxValue,
        minValue,
        minGasPrice,
        maxGasPrice,
        minPriorityFeePerGas,
        maxPriorityFeePerGas
      );

      logger.info(`Successfully created configuration: ${response}`)
      res.status(statusCodes.HTTP_OK).json(response);
    } catch (e) {
      logger.error(`An error occurred createConfig :: ${e}`);
    }
  }

  async updateConfiguration(req, res, next) {
    try {
      if (!req.params.id) {
        logger.error('Missing id from params!')
        res.status(statusCodes.HTTP_BAD_REQUEST).send({
          message: "Missing id from params!",
        });
      }

      const { id } = req.params;

      if (
        !req.body.maxValue ||
        !req.body.minValue ||
        !req.body.minGasPrice ||
        !req.body.maxGasPrice ||
        !req.body.minPriorityFeePerGas ||
        !req.body.maxPriorityFeePerGas
      ) {
        logger.error('Missing body params!')
        res.status(statusCodes.HTTP_BAD_REQUEST).send({
          message: "Missing body params!",
        });
      }

      const {
        maxValue,
        minValue,
        minGasPrice,
        maxGasPrice,
        minPriorityFeePerGas,
        maxPriorityFeePerGas,
      } = req.body;

      const response = await this.configService.updateConfiguration(
        id,
        maxValue,
        minValue,
        minGasPrice,
        maxGasPrice,
        minPriorityFeePerGas,
        maxPriorityFeePerGas
      );
      res.status(statusCodes.HTTP_OK).json(response);
    } catch (e) {
      logger.error(`An error occurred updateConfig :: ${e}`);
    }
  }

  async getConfigurations(req, res, next) {
    try {
      const response = await this.configService.getConfigurations();
      res.status(statusCodes.HTTP_OK).json(response);
    } catch (e) {
      logger.error(`An error occurred getConfig :: ${e}`);
    }
  }

  async softDeleteConfiguration(req, res, next) {
    try {
      if (!req.params.id) {
        logger.error('Missing id from params!')
        res.status(statusCodes.HTTP_BAD_REQUEST).send({
          message: "Missing id from params!",
        });
      }

      const { id } = req.params;
      const response = await this.configService.softDeleteConfiguration(id);
      res.status(statusCodes.HTTP_OK).json(response);
    } catch (e) {
      logger.error(`An error occurred deteleConfig :: ${e}`);
    }
  }

  //TODO implement hard delete using Sequelize CASCADE
  async hardDeleteConfiguration(req, res, next) {
    try {
      const response = this.configService.deleteConfiguration();
      res.status(statusCodes.HTTP_OK).json(response);
    } catch (e) {
      logger.error(`An error occurred deteleConfig :: ${e}`);
    }
  }
}
