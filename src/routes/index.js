import express from 'express';
import { container } from '../modules/containerInjection.module.js'
const router = express.Router();
const ConfigurationController = container.resolve('configurationController');
const ActiveConfigurationController = container.resolve('activeConfigurationController');

//CRUD OPERATIONS FOR DYNAMIC CONFIGURATIONS
router.get('/configurations', ConfigurationController.getConfigurations.bind(ConfigurationController));
router.post('/configuration/new', ConfigurationController.createConfiguration.bind(ConfigurationController));
router.put('/configuration/:id', ConfigurationController.updateConfiguration.bind(ConfigurationController));
router.delete('/configuration/:id', ConfigurationController.softDeleteConfiguration.bind(ConfigurationController));
//SET ACTIVE DYNAMIC CONFIGURATION
router.post('/setactive/:id', ActiveConfigurationController.setConfiguration.bind(ActiveConfigurationController));
export default router;