
import express from "express";
import * as dotenv from "dotenv";
import bodyParser from "body-parser";
import router from "./routes/index.js";
import winston from "winston";

dotenv.config();

class Server {
  app;
  port;
  EthTransactionService;
  constructor() {
    this.app = express();
    this.port = 3000;
    this.setup();
  }

  setup() {
    this.app.use(express.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use("/", router);
  }

  run() {
    this.server = this.app.listen(this.port, () => {
      winston.info(`Server is running on port :: ${this.port}`);
    });
  }

  stop(done) {
    this.server.close(done);
  }
}

export default Server;
