import { DataTypes, Sequelize } from "sequelize";
import { Transaction } from "./transaction.model.js";
import * as dotenv from "dotenv";
//required to user __dirname
import path from "path";
import { fileURLToPath } from "url";
import { ActiveConfiguration } from "./activeConfig.model.js";
//__dirname is not executable using es6 so we do this to use it
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
//add path to properly load the .env file
dotenv.config({ path: path.resolve(__dirname, "../../../.env") });

const sequelize = new Sequelize(
  `${process.env.DB}`,
  `${process.env.DB_USER}`,
  `${process.env.PASSWORD}`,
  {
    host: "localhost",
    dialect: "mariadb",
  }
);
export const Configuration = sequelize.define(
  "configuration",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4(),
      allowNull: false,
      primaryKey: true,
    },
    maxValue: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    minValue: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    minGasPrice: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    maxGasPrice: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    maxPriorityFeePerGas: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    minPriorityFeePerGas: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
  },
  {
    freezeTableName: true,
  }
);

Configuration.hasMany(Transaction);
Transaction.belongsTo(Configuration);


// Configuration.sync({ alter: true })
//   .then((data) => {
//     console.log(`Configuration Model synced successfully!`);
//   }).then((data) => {
//     Transaction.sync({alter: true})
//   }).then((data) => {
//     console.log(`Transaction Model synced successfully!`);
//   }).then((data) => {
//     ActiveConfiguration.sync({alter: true})
//   }).then((data) => {
//     console.log(`ActiveConfiguration Model synced successfully!`);
//   })
//   .catch((err) => {
//     console.error(`An error occurred while syncing DB models sync:: ${err}`);
//   });
