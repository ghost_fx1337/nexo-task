import { DataTypes, Sequelize } from "sequelize";
import * as dotenv from "dotenv";
//required to user __dirname
import path from "path";
import { fileURLToPath } from "url";
//__dirname is not executable using es6 so we do this to use it
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
//add path to properly load the .env file
dotenv.config({ path: path.resolve(__dirname, "../../../.env") });

const sequelize = new Sequelize(
  `${process.env.DB}`,
  `${process.env.DB_USER}`,
  `${process.env.PASSWORD}`,
  {
    host: "localhost",
    dialect: "mariadb",
  }
);

export const Transaction = sequelize.define(
  "transaction",
  {
    transaction_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    hash: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    blockHash: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    nonce: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    blockNumber: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    transactionIndex: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    value: {
      type: DataTypes.BIGINT,
      
    },
    gas: {
      type: DataTypes.BIGINT,
     
    },
    gasPrice: {
      type: DataTypes.BIGINT,
      
    },
    from: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    to: {
      type: DataTypes.STRING,
      
    },
    maxFeePerGas: {
      type: DataTypes.BIGINT,
     
    },
    maxPriorityFeePerGas: {
      type: DataTypes.STRING,
     
    },
    input: {
      type: DataTypes.TEXT('long'),
      
    }
  },
  { freezeTableName: true }
);

// Transaction.sync({ alter: true })
//   .then((data) => {
//     console.log(`Model synced successfully! ${data}`);
//   })
//   .catch((err) => {
//     console.error(`An error occurred Transaction Model sync:: ${err}`);
//   });

// Transaction model -- this is here only for reference
// {
//     "hash": "0x9fc76417374aa880d4449a1f7f31ec597f00b1f6f3dd2d66f4c9c6c445836d8b",
//     "nonce": 2,
//     "blockHash": "0xef95f2f1ed3ca60b048b4bf67cde2195961e0bba6f70bcbea9a2c4e133e34b46",
//     "blockNumber": 3,
//     "transactionIndex": 0,
//     "from": "0xa94f5374fce5edbc8e2a8697c15331677e6ebf0b",
//     "to": "0x6295ee1b4f6dd65047762f924ecd367c17eabf8f",
//     "value": '123450000000000000', AMOUNT OF ETH TO TRANSFER FROM SENDER TO RECIPIENT
//     "gas": 314159,
//     "gasPrice": '2000000000000',
//     "input": "0x57cb2fc4"
// }

// accessList: [],
// blockHash: '0x6bf2f150f6668a72fbafe28cdb4cad6180c38cef33463a6c9a4cba6ca647b49e',
// blockNumber: 15514221,
// chainId: '0x1',
// from: '0xE3Ad13f741AE5078034b9438EFb25bB5D15D9EF3',
// gas: 150667,
// gasPrice: '4497240729',
// hash: '0xbf5d13376b26c024c0a7f22353c2b7905c1f2f529ab9429806975be50a1be88e',
// input: '0x94bf804d0000000000000000000000000000000000000000000000106db43f5af65315210000000000000000000000003ee52d02edb15474b8657e4196da3fa69ca77aa1',
// maxFeePerGas: '9476486960',
// maxPriorityFeePerGas: '1000000000',
// nonce: 3129,
// r: '0x27f6c75d6be32df8a21c15526bc1e24ab97d39e368ecb1454bddf249ddca8e4b',
// s: '0x149b852b771053bbb1b3fc6760ae5be41cf2ccaac7b65758f3d764327777c1e1',
// to: '0x554d07E87cD569bD02b341d105F05F7E88CF29A2',
// transactionIndex: 14,
// type: 2,
// v: '0x1',
// value: '0'
