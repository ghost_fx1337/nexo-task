import { DataTypes, Sequelize } from "sequelize";
import * as dotenv from "dotenv";
//required to user __dirname
import path from "path";
import { fileURLToPath } from "url";
//__dirname is not executable using es6 so we do this to use it
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
//add path to properly load the .env file
dotenv.config({ path: path.resolve(__dirname, "../../../.env") });

const sequelize = new Sequelize(
  `${process.env.DB}`,
  `${process.env.DB_USER}`,
  `${process.env.PASSWORD}`,
  {
    host: "localhost",
    dialect: "mariadb",
  }
);

export const ActiveConfiguration = sequelize.define(
  "active_configuration",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4(),
      allowNull: false,
      primaryKey: true,
    },
    configurationId: {
      type: DataTypes.STRING,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
    }
  },
  { freezeTableName: true }
);

// ActiveConfiguration.sync({ alter: true })
//   .then((data) => {
//     console.log(`Model synced successfully! ${data}`);
//   })
//   .catch((err) => {
//     console.error(`An error occurred ActiveConfiguration Model sync:: ${err}`);
//   });
