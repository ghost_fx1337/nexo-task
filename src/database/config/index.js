import { Sequelize } from "sequelize";
import * as dotenv from "dotenv";
//required to user __dirname
import path from "path";
import { fileURLToPath } from "url";
//__dirname is not executable using es6 so we do this to use it
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
//add path to properly load the .env file
dotenv.config({ path: path.resolve(__dirname, "../../../.env") });

  const sequelize = new Sequelize(
  `${process.env.DB}`,
  `${process.env.DB_USER}`,
  `${process.env.PASSWORD}`,
  {
    host: "localhost",
    dialect: "mariadb",
  }
);

//This creates the table if it doesn't exist (and does nothing if it already exists)
// database.sync();

async function checkConnection() {
  try {
    await database.authenticate();
    console.log(`Connection has been established successfully!`);
  } catch (error) {
    console.error(`Unable to connect to the database:: ${error}`);
  }
}

//Check if we are successfully able to establish a connection with the DB server using the configured credentials
// checkConnection();
