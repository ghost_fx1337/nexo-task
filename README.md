**Introduction:**

This application monitors and watches all Ethereum transactions and stores and filters data based on a predefined dynamic configuration.

**Technologies/libraries used:**

- JavaScript
- Node
- ExpressJS
- MariaDB
- Sequelize
- Axios
- Awilix
- web3
- Infura
- Winston

**File structure:**

```tree -I node_modules -L 2```

```.
├── README.md
├── package-lock.json
├── package.json
├── src
│   ├── controllers
│   ├── database
│   ├── helpers
│   ├── index.js
│   ├── middlewares
│   ├── modules
│   ├── routes
│   ├── server.js
│   ├── services
│   └── types
└── transaction-watch-dog\ (1).md
```

**Start:**

Run <b>npm i</b> inside the project's root directory in order to install the required node_modules.

<b>NB:</b> You will need to create a .env file and populate the required variables to start the application. An example file can be found in the project root directory: 

```.env.example```

<b>NB: </b>The application requires a local MariaDB server! 

```https://mariadb.com/get-started-with-mariadb/```

You will need to create a database, user, assign that user to the database in question with the necessary privileges:
This can be done via terminal commands or using a workbench (MySQL Workbench for example)

Configure in the project .env file the following:

* database name
* user (must be assigned to that database with the necessary privileges)
* user password

In order to populate the empty database with the entities configured in this application run the below command:

```node ./src/database/models/configuration.model.js```

<b>NB:</b> Uncomment the following script in <b>./src/database/models/configuration.model.js</b> file before running the below command:

```
Configuration.sync({ alter: true })
  .then((data) => {
    console.log(`Configuration Model synced successfully!`);
  }).then((data) => {
    Transaction.sync({alter: true})
  }).then((data) => {
    console.log(`Transaction Model synced successfully!`);
  }).then((data) => {
    ActiveConfiguration.sync({alter: true})
  }).then((data) => {
    console.log(`ActiveConfiguration Model synced successfully!`);
  })
  .catch((err) => {
    console.error(`An error occurred while syncing DB models sync:: ${err}`);
  });
```

<b>node *./src/database/models/configuration.model.js*</b>

Make sure to comment the sync script back after running the command

The below script will start the express server:

```npm start```

<b>NB: </b>Please run the command from the source folder of the project. 

***Implemented Features***

* The application will automatically begin to track <i>blocks</i> / <i>block numbers</i> / <i>transactions</i> / <i>transaction count</i> once started.
<b>*NOTE*</b>: <b>*console.logs are left on purpose to display this activity within the terminal*</b>
* The application supports full CRUD operations for creating <b>Dynamic Configurations</b>
* The application will load the most recently used <b>Dynamic Configuration</b> when started (if started for the first time, a configuration needs to be created and set). <b>NOTE: the blockchain tracking uses a subscription to a webhook. The subscription's event emitter will only begin working when an active configuration has been loaded.</b>
* The application will filter all incoming transactions through the specifications of the active <b>Dynamic Configuration</b> and save any transactions which meets its requirements inside the database
* The application is able to switch the active <b>Dynamic Configuration</b> without restarting
* The applicaion keeps track of any actions / errors and logs them in console and in local files - combined.log, error.log

<b>!DISCLAIMER!</b>

```* ***Bonus*** Have additional Configuration to parse Transactions with delayed amount of Blocks.```

I was not able to understand this requirement and was not able to implement it.

***Endpoints:***

* Create Dynamic Configuration

<b>@Post</b>

```http://localhost:3000/configuration/new/```

body params example:

```
{
    "maxValue": "100000", 
    "minValue": "2000",
    "minGasPrice": "3000", 
    "maxGasPrice": "400", 
    "minPriorityFeePerGas": "50000", 
    "maxPriorityFeePerGas": "60000"
}
```
* Get all Dynamic Configurations
<b>@Get</b>

```http://localhost:3000/configurations```

* Update Dynamic Configuration

<b>@Put</b>

```http://localhost:3000/configuration/:id```

body params example:

```
{
    "maxValue": "100000", 
    "minValue": "2000",
    "minGasPrice": "3000", 
    "maxGasPrice": "400", 
    "minPriorityFeePerGas": "50000", 
    "maxPriorityFeePerGas": "60000"
}
```

request params:

```id```

* Soft delete Dynamic Configuration

<b>@Delete</b>

```http://localhost:3000/configuration/:id```

request params:

```id```

* Set Active Configuration Strategy

<b>@Post</b>

```http://localhost:3000/setactive/:id```

request params:

```id```

<b>!DISCLAIMER!</b>
This was very important for the DI to start working properly.
Calling services in controllers returned undefined because of router binding
[LinkToStackOverflow](https://github.com/jeffijoe/awilix/issues/88)

***Happy Path instructions:***

- npm start

**IF YOU HAVE ALREADY LOADED THE DB TABLES SKIP THE NEXT STEP**

**the next step needs to be performed only once**
- uncomment sync function in ./src/database/model/configuration.model.js, save the file and run 

```node src/database/model/configuration.model.js```

- Comment back the sync script and save the file

- POSTMAN => @POST Create configuration => http://localhost:3000/configuration/new/

Body params:

```{
    "maxValue": "11", 
    "minValue": "112",
    "minGasPrice": "3", 
    "maxGasPrice": "4", 
    "minPriorityFeePerGas": "5", 
    "maxPriorityFeePerGas": "6"
}
```

- In a workbench or via terminal => 

```SELECT * FROM configuration;``` => Get id

- POSTMAN => @POST Set active configuration =>

http://localhost:3000/setactive/76965172-1813-4c4b-896f-1f0dcb158659

* You have now loaded the active configuration and the event emitter will begin firing 

* POST / DELETE / UPDATE / CAN NOW BE USED

* SET another active configuration and you can confirm that the transaction table start saving data with a reference to the new configuration uuid

